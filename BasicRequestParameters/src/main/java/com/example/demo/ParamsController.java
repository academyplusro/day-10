package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParamsController {

	@RequestMapping("/hello")
	public String receiveUser(@RequestParam String name ) {
		String output = "Hello " + (name);
		return output;
	}
	
}
