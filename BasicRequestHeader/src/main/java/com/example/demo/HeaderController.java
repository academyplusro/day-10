package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class HeaderController {

	@RequestMapping(value = "/simpleHeader")
	public String testSingleRequestHeader(
			@RequestHeader(value = "Accept-Encoding", defaultValue = "en-US") String encoding,
			@RequestHeader("Accept-Language") String language){
 
		System.out.println("**************************");
		System.out.println("Simple Request Header Test");
		System.out.println("Accept-Encoding :: " + encoding);
		System.out.println("Accept-Language :: " + language);
		System.out.println("**************************");
 
		return "success";
    }
}