package com.example.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import com.example.model.Data;

@ComponentScan("com.example")
@Service
public class DataService {

	private List<Data> datas = Arrays.asList(new Data("Spring",1));
	
	public List<Data> getAllDatas(){
	return datas;
	}
}
