package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Data;
import com.example.service.DataService;


@RestController
public class DataController {

	@Autowired
	private DataService dataService;
	
	@RequestMapping("/datas")
    public List<Data> getAllDatas(){
		return dataService.getAllDatas();
    }
	
}
