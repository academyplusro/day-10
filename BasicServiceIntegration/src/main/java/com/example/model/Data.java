package com.example.model;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.example")
public class Data {

	private String name;
	private int id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Data(String name, int id) {
		super();
		this.name = name;
		this.id = id;
	}
	
	
}
