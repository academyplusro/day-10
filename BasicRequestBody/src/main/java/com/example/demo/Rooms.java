package com.example.demo;

public class Rooms {

	private String name;
	private String description;
	private long id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getid() {
		return id;
	}
	public void setid(long id) {
		this.id = id;
	}
	
	
	
}
