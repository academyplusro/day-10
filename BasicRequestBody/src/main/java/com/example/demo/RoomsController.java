package com.example.demo;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoomsController {

	@RequestMapping(value = "/rooms" , method = RequestMethod.GET)
	public ArrayList<Rooms>getroomList(){
		
		Rooms room1 = new Rooms();
		room1.setName("Single room");
		room1.setDescription("Beach view");
		room1.setid(0);
		Rooms room2 = new Rooms();
		room2.setName("Double rooms");
		room2.setDescription("Ocean view");
		room2.setid(1);
		Rooms room3 = new Rooms();
		room3.setName("Matrimonial room");
		room3.setDescription("City view");
		room3.setid(2);
		ArrayList<Rooms> roomList = new ArrayList<Rooms>();
		roomList.add(room1);
		roomList.add(room2);
		roomList.add(room3);
		
		return roomList;
 }
}